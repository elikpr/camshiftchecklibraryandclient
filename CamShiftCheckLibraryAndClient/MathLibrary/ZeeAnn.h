#ifndef ZEEANN_H
#define ZEEANN_H

#include <opencv2/core/core.hpp>

namespace ZeeAnn
{
	const int width = 640, height = 480;

	void connect_mono( int device, int exposure, double gain, bool gamma_encoding );

	void connect_stereo( int exposureR, int exposureL, double gain, bool gamma_encoding );

	void set_exposure( int device, int exposure );

	void set_gain( int device, double gain );

	void get_data( int device, uchar *buffer );

	void get_image( const uchar *buffer, int width, int height, int xoffset, int yoffset, cv::Mat &color_image );

	void disconnect( int device );

	void disconnect();
}

#endif