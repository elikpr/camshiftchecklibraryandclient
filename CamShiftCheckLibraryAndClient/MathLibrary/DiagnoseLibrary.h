#pragma once

#ifndef MATHLIBRARY_HPP
#define MATHLIBRARY_HPP

extern "C"
{
	__declspec(dllexport) int __stdcall calc_cam_shift(bool SHOW_IMAGE);
}

extern "C"
{
	__declspec(dllexport) void __stdcall imgshow();
}

extern "C"
{
	__declspec(dllexport) int __stdcall backgroundLearn();
}
#endif
