

#include <DShow.h>
#include "HontekSmartptr.h"
#pragma comment(lib, "strmiids.lib") 

class IMyCaptureGraphBuilder
{
public:
    IMyCaptureGraphBuilder()
    {
        VidPID_ = 0xE0;
        HRESULT hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, 
			IID_ICaptureGraphBuilder2, (void**)&graphBuilder2_ ); 
        //ASSERT( S_OK == hr );
    }
   
    void ReleaseFilters( )
    {
        pVideoPin_.Release();
    }

public:
   STDMETHOD(FindInterface)(const GUID *pCategory,
                          const GUID *pType,
                          IBaseFilter *pf,
                          REFIID riid,
                          void **ppint
                          );

    STDMETHOD(GetFiltergraph)( IGraphBuilder **ppfg );


    STDMETHOD(RenderStream)( const GUID *pCategory,
                          const GUID *pType,
                          IUnknown *pSource,
                          IBaseFilter *pIntermediate,
                          IBaseFilter *pSink
                          );

    STDMETHOD(SetFiltergraph)( IGraphBuilder *pfg );



protected:
    SmartPtr<ICaptureGraphBuilder2> graphBuilder2_;
    ULONG   VidPID_; 
private:
     SmartPtr<IPin>   pVideoPin_;

};
