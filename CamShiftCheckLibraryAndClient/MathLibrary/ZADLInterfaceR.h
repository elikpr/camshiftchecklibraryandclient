
#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif


__declspec(dllimport) void ZADRConnect(int ImageFormat);
__declspec(dllimport) void ZADRSensorInitial(int ImageFormat);
//0 Image_format_Bayer
//1 Image_format_Y
__declspec(dllimport) void ZADRDisConnect();
__declspec(dllimport) void ZADRGrabStart();
__declspec(dllimport) void ZADRGrabStop();
__declspec(dllimport) void ZADRgetImageHeight(unsigned int *m_ImageHeight);
__declspec(dllimport) void ZADRgetImageWidth(unsigned int *m_ImageWidth);
__declspec(dllimport) void ZADRgetLImageDump(unsigned char *ImageBuffer);

__declspec(dllimport) void ZADRAutoEnable(bool enable);
__declspec(dllimport) void ZADRExposureCtrl(int exposure);
//exposure max = 748
//exposure 748이 넘어가면 fps가 느려짐
__declspec(dllimport) void ZADRGainCtrl(double gain);
//Gain max = 31;
//Gain step = x1~x2 16step
//Gain step = x2~x4 16step
//Gain step = x4~x8 16step
//Gain step = x8~x16 16step
//Gain step = x16~x31 16step
__declspec(dllimport) void ZADRDGainCtrl(double gain);
//Gain max = 3.984375
//Gain step = 0.015625[1/64 resolution] 
__declspec(dllimport) void ZADRXMoveCtrl(int Startx);
//Startx default 106
//Startx range 0~213
__declspec(dllimport) void ZADRReadFirmwareVersion(char *name);
__declspec(dllimport) void ZADRReadSerialCode(char *name);
__declspec(dllimport) bool ZADRDownloadFirmware(char* Filename);
__declspec(dllimport) bool ZADRGPIOCtrl(int Port,int Value);
//Port = 0 : Reset
//Port = 1 : StandBy
//Value = 0 : low
//Value = 1 : high
__declspec(dllimport) void ZADRXOffset(int XOffset);
//Xoffset Range : 0~40
//640 center Xoffset 20[default]
__declspec(dllimport) void ZADRYOffset(int YOffset);
//Yoffset Range : 0~80
//480 center Yoffset 40[default]
__declspec(dllimport) void ZADRFlipMirror(bool Flip, bool Mirror);
//Flip = TRUE(Flip=on), FALSE(Flip=off)
//Mirror = TRUE(Mirror=on), FALSE(Mirror=off)
__declspec(dllimport) void ZADRi2cRead(char addr, char* value);
//i2c register Read
//addr - address
//value - read value
//addr, value = 8bit data
__declspec(dllimport) void ZADRi2cWrite(char addr, char value);
//i2c register Write
//addr - address
//value - write value
//addr, value = 8bit data
//pageA setting - addr=0x00, value=0x00 Write
//pageB setting - addr=0x00, value=0x01 Write
//pageC setting - addr=0x00, value=0x02 Write
//pageD setting - addr=0x00, value=0x03 Write
#ifdef __cplusplus
}
#endif

