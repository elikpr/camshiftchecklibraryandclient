    
#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif

	
__declspec(dllimport) void ZADLConnect(int ImageFormat);
__declspec(dllimport) void ZADLSensorInitial(int ImageFormat);
//0 Image_format_Bayer
//1 Image_format_Y
__declspec(dllimport) void ZADLDisConnect();
__declspec(dllimport) void ZADLGrabStart();
__declspec(dllimport) void ZADLGrabStop();
__declspec(dllimport) void ZADLgetImageHeight(unsigned int *m_ImageHeight);
__declspec(dllimport) void ZADLgetImageWidth(unsigned int *m_ImageWidth);
__declspec(dllimport) void ZADLgetLImageDump(unsigned char *ImageBuffer);

__declspec(dllimport) void ZADLAutoEnable(bool enable);
__declspec(dllimport) void ZADLExposureCtrl(int exposure);
//exposure max = 748
//exposure 748이 넘어가면 fps가 느려짐
__declspec(dllimport) void ZADLGainCtrl(double gain);
//Gain max = 31;
//Gain step = x1~x2 16step
//Gain step = x2~x4 16step
//Gain step = x4~x8 16step
//Gain step = x8~x16 16step
//Gain step = x16~x31 16step
__declspec(dllimport) void ZADLDGainCtrl(double gain);
//Gain max = 3.984375
//Gain step = 0.015625[1/64 resolution] 
__declspec(dllimport) void ZADLXMoveCtrl(int Startx);
//Startx default 106
//Startx range 0~213
__declspec(dllimport) void ZADLReadFirmwareVersion(char *name);
__declspec(dllimport) void ZADLReadSerialCode(char *name);
__declspec(dllimport) bool ZADLDownloadFirmware(char* Filename);
__declspec(dllimport) bool ZADLGPIOCtrl(int Port,int Value);
//Port = 0 : Reset
//Port = 1 : StandBy
//Value = 0 : low
//Value = 1 : high
__declspec(dllimport) void ZADLXOffset(int XOffset);
//Xoffset Range : 0~40
//640 center Xoffset 20[default]
__declspec(dllimport) void ZADLYOffset(int YOffset);
//Yoffset Range : 0~80
//480 center Yoffset 40[default]
__declspec(dllimport) void ZADLFlipMirror(bool Flip, bool Mirror);
//Flip = TRUE(Flip=on), FALSE(Flip=off)
//Mirror = TRUE(Mirror=on), FALSE(Mirror=off)
__declspec(dllimport) void ZADLi2cRead(char addr, char* value);
//i2c register Read
//addr - address
//value - read value
//addr, value = 8bit data
__declspec(dllimport) void ZADLi2cWrite(char addr, char value);
//i2c register Write
//addr - address
//value - write value
//addr, value = 8bit data
//pageA setting - addr=0x00, value=0x00 Write
//pageB setting - addr=0x00, value=0x01 Write
//pageC setting - addr=0x00, value=0x02 Write
//pageD setting - addr=0x00, value=0x03 Write
#ifdef __cplusplus
}
#endif
