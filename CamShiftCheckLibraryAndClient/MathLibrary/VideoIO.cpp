#include <iostream>
#include "ZeeAnn.h"
#include "VideoIO.h"
#include "HontekCameraSetting.h"
#include <opencv2\imgproc\imgproc.hpp>

using std::cout;

DWORD WINAPI writing_thread( LPVOID param )
{
	VideoWriter::ThreadData *p = (VideoWriter::ThreadData*) param;

	std::string filename = p->filename;
	int width = p->width;
	int height = p->height;
	int num_images = p->num_images;
	cv::Mat *images = p->images;
	bool *image_exist = p->image_exist;

	std::ofstream fout;
	fout.open( filename.c_str(), std::ios_base::binary );
	fout.write( (char*) &width,  sizeof( int ) );
	fout.write( (char*) &height, sizeof( int ) );
	
	int image_pos = 0;

	while( p->run )
	{
		while( image_exist[ image_pos ] )
		{
			fout.write( (char*) images[image_pos].data, images[image_pos].channels() * width * height );
			image_exist[ image_pos ] = false;
			image_pos = ++image_pos % num_images;
		}
		
		Sleep( 10 );
	}

	fout.close();

	return 0;
}

VideoWriter::VideoWriter() : thread_handle( NULL ) {}

VideoWriter::VideoWriter( VideoKind video_kind, const char file[], int width, int height, double fps )
{
	open( video_kind, file, width, height, fps );
}

void VideoWriter::open( VideoKind video_kind, const char file[], int width, int height, double fps )
{
	if( is_open() ) return;

	VideoWriter::video_kind = video_kind;
	VideoWriter::width = width;
	VideoWriter::height = height;
	
//	int fourcc = -1;
	int fourcc = CV_FOURCC('X','2','6','4');
	
	switch( video_kind )
	{
	case SIMPLE:
		images = new cv::Mat[ num_images ];
		image_exist = new bool[ num_images ];
		for( int i = 0; i < num_images; ++i ) { images[i].create( height, width, CV_8UC3 ); image_exist[i] = false; }
		image_pos = 0;
		thread_data.run = true;
		thread_data.filename = file;
		thread_data.width = width;
		thread_data.height = height;
		thread_data.num_images = num_images;
		thread_data.images = images;
		thread_data.image_exist = image_exist;
		thread_handle = CreateThread( NULL, 0, writing_thread, &thread_data, 0, NULL );
		break;
	case STANDARD:
		cv_video_writer.open( file, fourcc, fps, cv::Size( width, height ), true );
		break;
	default:
		std::cerr << "Unknown video type\n";
		exit( EXIT_FAILURE );
	}
}

bool VideoWriter::is_open()
{
	return thread_handle != NULL || cv_video_writer.isOpened();
}

bool VideoWriter::write_frame( const cv::Mat &image )
{
	switch( video_kind )
	{
	case SIMPLE:
		if( image_exist[ image_pos ] ) { std::cerr << "Error: image buffer full\n"; return false; }
		image.copyTo( images[ image_pos ] );
		image_exist[ image_pos ] = true;
		image_pos = ++image_pos % num_images;
		break;
	case STANDARD:
		cv_video_writer.write( image );
		break;
	}

	return true;
}

void VideoWriter::close()
{
	switch( video_kind )
	{
	case SIMPLE:
		if( thread_handle != NULL )
		{
			thread_data.run = false;
			WaitForSingleObject( thread_handle, INFINITE );
			CloseHandle( thread_handle );
			thread_handle = NULL;
			delete [] images;
			delete [] image_exist;
		}
		break;
	case STANDARD:
		if( cv_video_writer.isOpened() ) cv_video_writer.release();
		break;
	}
}

VideoWriter::~VideoWriter()
{
	if( thread_handle != NULL ) close();
}


bool VideoReader::stereo = false;
bool VideoReader::buffer_empty = true;
VideoReader *VideoReader::last_one = NULL;

void VideoReader::set_stereo() { stereo = true; }

VideoReader::VideoReader() {}

VideoReader::VideoReader( VideoKind video_kind, int device, int camera_width, int camera_height, int xoffset, int yoffset,
                          int image_width, int image_height, int exposure, double gain, bool gamma_encoding )
{
	open( video_kind, device, camera_width, camera_height, xoffset, yoffset, image_width, image_height, exposure, gain, gamma_encoding );
}

VideoReader::VideoReader( VideoKind video_kind, const char file[] )
{
	open( video_kind, file );
}

void VideoReader::open( VideoKind video_kind, int device, int camera_width, int camera_height, int xoffset, int yoffset,
                        int image_width, int image_height, int exposure, double gain, bool gamma_encoding )
{
	VideoReader::video_kind     = video_kind;
	VideoReader::device         = device;
	VideoReader::camera_width   = camera_width;
	VideoReader::camera_height  = camera_height;
	VideoReader::xoffset        = xoffset;
	VideoReader::yoffset        = yoffset;
	VideoReader::width          = image_width;
	VideoReader::height         = image_height;
	VideoReader::exposure       = exposure;
	VideoReader::gain           = gain;
	VideoReader::gamma_encoding = gamma_encoding;
	VideoReader::eof            = false;
	real_time                   = true;
	frame_number                = 0;
	no_update_count             = 0;
	camera_size                 = cv::Size( camera_width, camera_height );

	if( width  <= 0 ) width  = camera_width;
	if( height <= 0 ) height = camera_height;

	if( device == -1 ) return;

	switch( video_kind )
	{
	case STANDARD:
	case HONTEK:

		capture.open( device );

		if( capture.isOpened() )
		{
			capture.set( CV_CAP_PROP_FRAME_WIDTH, camera_width );
			capture.set( CV_CAP_PROP_FRAME_HEIGHT, camera_height );
			capture.read(temp_image);
			if( temp_image.rows == camera_height && temp_image.cols == camera_width )
			{
				if( exposure != INT_MAX ) set_exposure( exposure );
			}
			else
			{
				eof = true;
			}
		}
		else eof = true;

		break;

	case ZEEANN:

		if( stereo )
		{
			if( last_one )
			{
				the_other = last_one;
				last_one->the_other = this;

				int exposureR, exposureL;

				if( device == 0 )
				{
					exposureR = exposure;
					exposureL = the_other->exposure;
				}
				else
				{
					exposureL = exposure;
					exposureR = the_other->exposure;
				}

				ZeeAnn::connect_stereo( exposureR, exposureL, gain, gamma_encoding );
			}
		}
		else
		{
			ZeeAnn::connect_mono( device, exposure, gain, gamma_encoding );
		}

		break;

	default:
		std::cerr << "Unknown video type\n";
		exit( EXIT_FAILURE );
	}

	last_one = this;
}

void VideoReader::open( VideoKind video_kind, const char file[] )
{
	VideoReader::video_kind = video_kind;
	VideoReader::eof        = false;
	real_time               = false;
	frame_number = 0;

	switch( video_kind )
	{
	case SIMPLE_FILE:
		fin.open( file, std::ios_base::binary );
		fin.seekg( 0, fin.end );
	    file_length = (unsigned long) fin.tellg();
		fin.seekg( 0, fin.beg );
		fin.read( (char*) &width,  sizeof( int ) );
		fin.read( (char*) &height, sizeof( int ) );
		break;
	case STANDARD_FILE:
		capture.open( file );
		width = (int) capture.get( CV_CAP_PROP_FRAME_WIDTH );
		height = (int) capture.get( CV_CAP_PROP_FRAME_HEIGHT );
		break;
	default:
		std::cerr << "Unkown video type\n";
		exit( EXIT_FAILURE );
	}

	last_one = this;
}

bool VideoReader::reset()
{
	close();
	open( video_kind, device, camera_width, camera_height, xoffset, yoffset, width, height, exposure, gain, gamma_encoding );
	return is_open();
}

bool VideoReader::is_open()
{
	return fin.is_open() || capture.isOpened() || device == -1;
}

void VideoReader::set_exposure( int exposure )
{
	VideoReader::exposure = exposure;

	if( video_kind == STANDARD )
	{
		capture.set( CV_CAP_PROP_EXPOSURE, exposure );
	}
	else if( video_kind == HONTEK )
	{
		CameraSet( device, exposure );
		
	}
	else if( video_kind == ZEEANN )
	{
		ZeeAnn::set_exposure( device, exposure );
	}
	else
	{
		std::cerr << "video_kind " << video_kind << " does not support set_exposure() function.\n";
		exit( EXIT_FAILURE );
	}
}

void VideoReader::set_gain( double gain )
{
	if( video_kind == ZEEANN )
	{
		ZeeAnn::set_gain( device, gain );
	}
	else
	{
		std::cerr << "video_kind " << video_kind << " does not support set_gain() function.\n";
		exit( EXIT_FAILURE );
	}
}

int VideoReader::get_width() { return width; }

int VideoReader::get_height() { return height; }

int VideoReader::get_rows() { return height; }

int VideoReader::get_cols() { return width; }

unsigned long VideoReader::get_frame_number()
{
	switch( video_kind )
	{
	case STANDARD_FILE:
		return (unsigned long) capture.get( CV_CAP_PROP_POS_FRAMES );
	default:
		return frame_number;
	}
}

bool VideoReader::grab_frame()
{
	if( device != -1 )
	{
		switch( video_kind )
		{
		case STANDARD:
		case HONTEK:
		case STANDARD_FILE:
			eof = ! capture.grab();
			break;
		case SIMPLE_FILE:
			fin.seekg( sizeof( char ) * 3 * width * height, fin.cur );
			eof = ( (unsigned long ) fin.tellg() >= file_length );
			break;
		default:
			std::cerr << "grab is not supported for this video type\n";
			exit( -1 );
		}
	}

	++frame_number;

	return !eof;
}

bool VideoReader::retrieve_frame( cv::Mat &image )
{
	if( device != -1 )
	{
		switch( video_kind )
		{
		case STANDARD:
		case HONTEK:
			capture.retrieve( temp_image );
			temp_image( cv::Range( yoffset, yoffset + height ), cv::Range( xoffset, xoffset + width ) ).copyTo( image );
			break;
		case STANDARD_FILE:
			capture.retrieve( temp_image );
			temp_image.copyTo( image );
			break;
		case SIMPLE_FILE:
			fin.seekg( - (int) sizeof( char ) * 3 * width * height, fin.cur );
			fin.read( (char*) image.data, sizeof( char ) * image.channels() * width * height );
			eof = fin.eof();
			break;
		default:
			std::cerr << "retrieve is not supported for this video type\n";
			exit( -1 );
		}
	}
	else image.setTo( 0 );

	return !eof;
}

bool VideoReader::read_frame( cv::Mat &image )
{
	if( device != -1 )
	{
		int64 tick0, tick1;

		switch( video_kind )
		{
		case STANDARD:
		case HONTEK:

			tick0 = cv::getTickCount();
			capture.read( temp_image );
			tick1 = cv::getTickCount();
			if( temp_image.size() != camera_size ) { cout << "Wrong image size\n"; eof = true; return false; }

			if( ( tick1 - tick0 ) / cv::getTickFrequency() > 0.9 )
			{
				cout << "cam" << device << ": delay in reading image\n";
				cv::Mat prev_image, diff_image, diff_image_gray;

				temp_image.copyTo( prev_image );
				capture.read( temp_image );
				if( temp_image.size() != camera_size ) { cout << "Wrong image size\n"; eof = true; return false; }

				cv::absdiff( prev_image, temp_image, diff_image );
				cv::cvtColor( diff_image, diff_image_gray, CV_BGR2GRAY );
				if( cv::countNonZero( diff_image_gray ) == 0 )
				{
					cout << "cam" << device << ": image not updated\n";
					if( ++no_update_count == 1 ) { eof = true; return false; }
				}
				else no_update_count = 0;
			}
			else no_update_count = 0;

			temp_image( cv::Range( yoffset, yoffset + height ), cv::Range( xoffset, xoffset + width ) ).copyTo( image );
			break;
	
		case ZEEANN:

			if( stereo )
			{
				if( buffer_empty )
				{
					buffer = new uchar[ ZeeAnn::width * ZeeAnn::height * 3 ];
					the_other->buffer = new uchar[ ZeeAnn::width * ZeeAnn::height * 3 ];

					ZeeAnn::get_data( device, buffer );
					ZeeAnn::get_data( 1 - device, the_other->buffer );
				}

				ZeeAnn::get_image( buffer, width, height, xoffset, yoffset, image );

				if( buffer_empty )
				{
					buffer_empty = false;
				}
				else
				{
					delete [] buffer;
					delete [] the_other->buffer;
					buffer_empty = true;
				}
			}
			else
			{
				buffer = new uchar[ ZeeAnn::width * ZeeAnn::height * 3 ];
				ZeeAnn::get_data( device, buffer );
				ZeeAnn::get_image( buffer, width, height, xoffset, yoffset, image );
				delete [] buffer;
			}
			break;

		case STANDARD_FILE:

			eof = ! capture.read( temp_image );
			temp_image.copyTo( image );
			break;

		case SIMPLE_FILE:

			fin.read( (char*) image.data, sizeof( char ) * image.channels() * width * height );
			eof = fin.eof();
			break;

		default:
			std::cerr << "Unknown video type\n";
			exit( -1 );
		}
	}
	else image.setTo( 0 );

	++frame_number;

	return !eof;
}

bool VideoReader::is_real_time() { return real_time; }

bool VideoReader::eos() { return eof; }

void VideoReader::close()
{
	switch( video_kind )
	{
	case HONTEK:
		FreeCapFilters();
	case STANDARD:
	case STANDARD_FILE:
		if( capture.isOpened() ) capture.release();
		break;
	case ZEEANN:
		if( stereo )
		{
			if( ! last_one ) ZeeAnn::disconnect();
			last_one = NULL;
		}
		else
		{
			ZeeAnn::disconnect( device );
		}
		break;
	case SIMPLE_FILE:
		if( fin.is_open() ) fin.close();
		break;
	}

	frame_number = 0;
}

VideoReader::~VideoReader()
{
	switch( video_kind )
	{
	case HONTEK:
		FreeCapFilters();
		break;
	case ZEEANN:
		if( stereo )
		{
			if( ! last_one ) ZeeAnn::disconnect();
			last_one = NULL;
		}
		else
		{
			ZeeAnn::disconnect( device );
		}
		break;
	case SIMPLE_FILE:
		fin.close();
		break;
	}
}