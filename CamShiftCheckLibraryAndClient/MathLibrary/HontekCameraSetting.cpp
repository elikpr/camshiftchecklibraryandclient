#ifndef WINVER  
#define WINVER 0x0700  
#endif

#pragma comment( lib, "HTCLibV10.lib" )

// YN #define _AFXDLL
// YN #include <afxwin.h>//#include <string.h>//<afxstr.h>
#include <Windows.h>
#include <control.h>
#include <strmif.h>
#include <opencv/highgui.h>
#include "HontekHVRLibEx.h"
#include "HontekCaptureGraph.h"
#include "HontekCameraSetting.h"

IAMCameraControl  *pCameraControl = NULL;
IAMVideoProcAmp   *pVideoControl = NULL;
LONG	lGreenGainDefault;
LONG	lRedGainDefault;
LONG	lBlueGainDefault;
LONG	wregData;
LONG	lGGainDefault;
LONG	lvalue;

// YN CString strInfo[20];   // Infos into our Listbox
int iIndex=0;
//CListBox m_ctrlListDevices;
#define MODE_1_EXPOSURE	4
#define MODE_1_SHUTTER	5
#define MODE_1_HBLANK	6
#define MODE_1_VBLANK	7
#define MODE_1_TRIGGER	8
#define MODE_1_USBSPEED	9
#define MODE_1_AEG		10
#define MODE_1_READ		11
#define MODE_1_WRITE	12
#define MODE_2_GAIN		9

struct _capstuff
{
    IMyCaptureGraphBuilder	*pBuilder;
    IVideoWindow			*pVW;
    IMediaEventEx			*pME;
    IAMVideoCompression		*pVC;
    IAMStreamConfig			*pVSC;      
    IBaseFilter				*pRender;
    IBaseFilter				*pVCap; 
    IGraphBuilder			*pFg;
    IMoniker				*rgpmVideoMenu[5];
    IMoniker				*pmVideo;
    BOOL fCaptureGraphBuilt;
    BOOL fPreviewGraphBuilt;
    BOOL fCapturing;
    BOOL fPreviewing;
    bool fDeviceMenuPopulated;
    WCHAR wachFriendlyName[120];
    double FrameRate;
	BOOL fUseFrameRate;
    BOOL fPreviewFaked;     
    int iNumVCapDevices;    // number of devices 
} gx;


void EnumerateAllDevices();
BOOL InitCapFilters();
void FreeCapFilters();
void TearDownGraph();

BOOL StartPreview();
BOOL StopPreview();
HWND ghwndApp=0; 
HWND m_hWnd;            // must be first data member
BOOL BuildPreviewGraph();




void IMonRelease(IMoniker *&pm)
{
    if(pm)
    {
        pm->Release();
        pm = 0;
    }
}

void EnumerateAllDevices()
{
UINT    uIndex = 0;
HRESULT hr;

    if(gx.fDeviceMenuPopulated)  return;
    gx.fDeviceMenuPopulated = true;
    gx.iNumVCapDevices      = 0;
   
     for(int i = 0; i < NUMELMS(gx.rgpmVideoMenu); i++)
						IMonRelease(gx.rgpmVideoMenu[i]);
    
	hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);

    ICreateDevEnum *pDevEnum=0;
    hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,IID_ICreateDevEnum, (void**)&pDevEnum);
    if(hr != NOERROR)
    {
 // YN  strInfo[iIndex++].Format(TEXT("Error Creating Device Enumerator"));
		iIndex++; // YN
       return;
    }

    IEnumMoniker *pEnum=0;
    hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnum, 0);
    if(hr != NOERROR)
    {
// YN   strInfo[iIndex++].Format(TEXT("!!!  No webcam found !!!"));
		iIndex++; // YN
        goto ENUM_EXIT;
    }

    
    pEnum->Reset();
    ULONG cFetched;
    IMoniker *pM;

    while(1)
    {
		hr = pEnum->Next(1, &pM, &cFetched);
		if(hr != S_OK) goto ENUM_EXIT;

        IPropertyBag *pBag=0;

        hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
        if(SUCCEEDED(hr))
        {
            VARIANT var;
            var.vt = VT_BSTR;
            hr = pBag->Read(L"FriendlyName", &var, NULL);
            if(hr == NOERROR)
            {
//	YN			strInfo[iIndex++];//.Format("%S", var.bstrVal);
				iIndex++; // YN
                SysFreeString(var.bstrVal);

                //ASSERT(gx.rgpmVideoMenu[uIndex] == 0);
                gx.rgpmVideoMenu[uIndex]        = pM;
                pM->AddRef();
            }
            pBag->Release();
        }
        pM->Release();
        uIndex++;
    }
    pEnum->Release();

	gx.iNumVCapDevices = uIndex;

ENUM_EXIT:
	pDevEnum->Release();
}

void StartThisDevice(IMoniker *pmVideo)  
{
     if(gx.pmVideo != pmVideo)
    {
        if(pmVideo)
        {
            pmVideo->AddRef();
        }

        IMonRelease(gx.pmVideo);
 
        gx.pmVideo = pmVideo;

       if(gx.fPreviewing)
            StopPreview();

        if(gx.fCaptureGraphBuilt || gx.fPreviewGraphBuilt)
            TearDownGraph();

		FreeCapFilters();		

        InitCapFilters();

		//gx.pVCap->QueryInterface(IID_IAMVideoProcAmp,(void**)&pVideoControl);
		//HVR_Set_ISP2(pVideoControl, MODE_2_GAIN, 10, 0);
        
		gx.pVCap->QueryInterface(IID_IAMCameraControl,(void**)&pCameraControl);	
	//RED
	wregData = (lRedGainDefault / 0x40) * 0x100 + (lRedGainDefault % 0x3f);
	HVR_Set_ISP(pCameraControl, 1, wregData, (LONG *)&wregData, 0, 0, 0, 0);
	//GREEN
	wregData = (lGreenGainDefault / 0x40) * 0x100 + (lGreenGainDefault % 0x3f);
	HVR_Set_ISP(pCameraControl, 2, wregData, (LONG *)&wregData, 0, 0, 0, 0);
	//RED
	wregData = (lBlueGainDefault / 0x40) * 0x100 + (lBlueGainDefault % 0x3f);
	HVR_Set_ISP(pCameraControl, 3, wregData, (LONG *)&wregData, 0, 0, 0, 0);
	//EXPOSURE
	HVR_Set_ISP(pCameraControl, MODE_1_EXPOSURE, lvalue, 0, 0, 0, 0, 0);
		BuildPreviewGraph();
        //StartPreview();                  
    }
}

BOOL InitCapFilters()
{
    HRESULT hr=S_OK;

    if(gx.pBuilder)    return TRUE;

	//====================== STEP_05 ==============================

    gx.pBuilder = new IMyCaptureGraphBuilder();
    if(gx.pBuilder == NULL )
    {
// YN	strInfo[iIndex++];;//.Format("Cannot instantiate graph builder");
		iIndex++; // YN
        return FALSE;
    }

     gx.pVCap = NULL;
// YN   int ii,xx=0;
// YN	CString strFriendlyName;

	
    if(gx.pmVideo != 0)
    {
        IPropertyBag *pBag;
        gx.wachFriendlyName[0] = 0;

        hr = gx.pmVideo->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
        if(SUCCEEDED(hr))
        {
            VARIANT var;
            var.vt = VT_BSTR;

            hr = pBag->Read(L"FriendlyName", &var, NULL);
            if(hr == NOERROR)
            {
             //strFriendlyName.Format("%s",var.bstrVal);
// YN		 ii = strFriendlyName.GetLength();
// YN		 while(xx < ii) {gx.wachFriendlyName[xx]= (WCHAR) strFriendlyName.GetAt(xx); xx++;}
             SysFreeString(var.bstrVal);
            }
            pBag->Release();
        }
        gx.pmVideo->BindToObject(0, 0, IID_IBaseFilter, (void**)&gx.pVCap);
    }

    if(gx.pVCap == NULL)
    {
// YN  strInfo[iIndex++];//.Format(TEXT("Error %x: Cannot create video capture filter"), hr);
		iIndex++; // YN
     goto INIT_FILTERS_FAIL;
    }


  
    if(gx.pFg)     return TRUE;

    hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC,IID_IGraphBuilder, (LPVOID *)&gx.pFg);

    hr = gx.pBuilder->SetFiltergraph(gx.pFg);
    if(hr != NOERROR)
    {
// YN strInfo[iIndex++];//.Format(TEXT("Cannot give graph to builder"));
		iIndex++; // YN
    goto INIT_FILTERS_FAIL;
    }

    // Add the video capture filter to the graph with its friendly name
    hr = gx.pFg->AddFilter(gx.pVCap, gx.wachFriendlyName);
    if(hr != NOERROR)
    {
// YN  strInfo[iIndex++];//.Format(TEXT("Error %x: Cannot add vidcap to filtergraph"), hr);
		iIndex++; // YN
    goto INIT_FILTERS_FAIL;
    }

       
    hr = gx.pBuilder->FindInterface(&PIN_CATEGORY_CAPTURE,&MEDIATYPE_Video, gx.pVCap,
                                          IID_IAMStreamConfig, (void **)&gx.pVSC);
  	if(hr == S_OK)  return TRUE;


INIT_FILTERS_FAIL:
    FreeCapFilters();
    return FALSE;
}

void FreeCapFilters()
{
  	if(gx.pFg) gx.pFg->Release();	gx.pFg = 0;

    if( gx.pBuilder )
    {
        delete gx.pBuilder;
        gx.pBuilder = NULL;
    }

	
    if(gx.pVCap) gx.pVCap->Release();  gx.pVCap = 0;
    if(gx.pVSC)  gx.pVSC->Release();   gx.pVSC  = 0;
    if(gx.pVC)   gx.pVC->Release();    gx.pVC   = 0;
}


void TearDownGraph()
{
  	if(gx.pRender) gx.pRender->Release(); gx.pRender = 0;
    if(gx.pME) gx.pME->Release();	gx.pME = 0;

    if(gx.pVW)
    {
        gx.pVW->put_Owner(NULL);			// stop drawing in our window
        gx.pVW->put_Visible(OAFALSE);
        gx.pVW->Release();
        gx.pVW = NULL;
    }

    if(gx.pVCap)
        gx.pBuilder->ReleaseFilters();

    gx.fCaptureGraphBuilt = FALSE;
    gx.fPreviewGraphBuilt = FALSE;
}


BOOL StartPreview()
{
    if(gx.fPreviewing)
        return TRUE;

    if(!gx.fPreviewGraphBuilt)
        return FALSE;

    // run the graph
    IMediaControl *pMC = NULL;
    HRESULT hr = gx.pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
    if(SUCCEEDED(hr))
    {
        hr = pMC->Run();
        if(FAILED(hr))
        {
            // stop parts that ran
            pMC->Stop();
        }
        pMC->Release();
    }
    if(FAILED(hr))
    {
// YN   strInfo[iIndex++].Format(TEXT("Error %x: Cannot run preview graph"), hr);
		iIndex++; // YN
        return FALSE;
    }

    gx.fPreviewing = TRUE;
    return TRUE;
}



BOOL StopPreview()
{
    if(!gx.fPreviewing)
		return FALSE;
 
    // stop the graph
    IMediaControl *pMC = NULL;
    HRESULT hr = gx.pFg->QueryInterface(IID_IMediaControl, (void **)&pMC);
    if(SUCCEEDED(hr))
    {
        hr = pMC->Stop();
        pMC->Release();
    }
    if(FAILED(hr))
    {
// YN   strInfo[iIndex++].Format(TEXT("Error %x: Cannot stop preview graph"), hr);
		iIndex++; // YN
        return FALSE;
    }

    gx.fPreviewing = FALSE;
    return TRUE;
}


BOOL BuildPreviewGraph()
{
//    int cy, cyBorder;
    HRESULT hr;
 			
            // maybe it's DV?
            hr = gx.pBuilder->RenderStream(&PIN_CATEGORY_PREVIEW,&MEDIATYPE_Video, gx.pVCap, NULL, NULL);
            if(hr == VFW_S_NOPREVIEWPIN)
            {
                // preview was faked up for us using the (only) capture pin
                gx.fPreviewFaked = TRUE;
            }
            else if(hr != S_OK)
            {
// YN           strInfo[iIndex++].Format(TEXT("!!! This graph cannot preview !!!"));
				iIndex++; // YN
                gx.fPreviewGraphBuilt = FALSE;
                return FALSE;
            }


  
    hr = gx.pFg->QueryInterface(IID_IVideoWindow, (void **)&gx.pVW);
	/*
    if(hr != NOERROR)
    {
        strInfo[iIndex++].Format(TEXT("!!! This graph cannot preview properly !!!"));
    }
    else
    {
        RECT rc;
        gx.pVW->put_Owner((OAHWND)ghwndApp);    // We own the window now
        gx.pVW->put_WindowStyle(WS_CHILD);      // you are now a child

        // give the preview window all our space but where the status bar is
        GetClientRect(ghwndApp, &rc);
        cyBorder = GetSystemMetrics(SM_CYBORDER);
        cy = 150 + cyBorder;
        rc.bottom -= cy;

        gx.pVW->SetWindowPosition(0, 0, rc.right, rc.bottom); // be this big
        gx.pVW->put_Visible(OATRUE);
    }

*/
    gx.fPreviewGraphBuilt = TRUE;
    return TRUE;
}

void CameraSet(int number,int expose)
{
	lRedGainDefault = 39;
	lGreenGainDefault = 30;
	lBlueGainDefault = 62;
	//lGGainDefault = gain;
	lvalue =  expose;

	EnumerateAllDevices();
	//	EnumerateAllDevices();
// YN m_ctrlListDevices.ResetContent();
	
	int ii=0;
// YN while(ii < iIndex)	m_ctrlListDevices.AddString(strInfo[ii++]);

	//=============================================
	//ghwndApp = m_hWnd;//this->m_hWnd;
	//gx.FrameRate = 130.0;
	//gx.pmVideo   = 0;

	StartThisDevice(gx.rgpmVideoMenu[number]);			
}