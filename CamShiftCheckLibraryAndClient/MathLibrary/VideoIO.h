#ifndef VIDEOIO_H
#define VIDEOIO_H

#include <windows.h>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>

class VideoWriter
{
public:

	enum VideoKind { SIMPLE, STANDARD };

	struct ThreadData
	{
		bool run;
		std::string filename;
		int width;
		int height;
		int num_images;
		cv::Mat *images;
		bool *image_exist;
	};

	VideoWriter();
	VideoWriter( VideoKind video_kind, const char file[], int width, int height, double fps = 0 );
	void open( VideoKind video_kind, const char file[], int width, int height, double fps = 0 );
	bool is_open();
	bool write_frame( const cv::Mat &image );
	void VideoWriter::close();
	~VideoWriter();

private:

	VideoKind video_kind;
	int width, height;
	cv::VideoWriter cv_video_writer;
	const static int num_images = 60;
	cv::Mat *images;
	bool *image_exist;
	int image_pos;
	ThreadData thread_data;
	HANDLE thread_handle;
};

class VideoReader
{
public:
	enum VideoKind { STANDARD, HONTEK, ZEEANN, SIMPLE_FILE, STANDARD_FILE };
private:
	static bool stereo;
	static bool buffer_empty;
	static VideoReader *last_one;
	cv::VideoCapture capture;
	std::ifstream fin;
	cv::Mat temp_image;
	uchar *buffer;
	bool eof;
	bool real_time;
	VideoReader *the_other;
	unsigned long frame_number, file_length, no_update_count;
public:
	VideoKind video_kind;
	cv::Size camera_size;
	int device, camera_width, camera_height, xoffset, yoffset, width, height, exposure;
	bool gamma_encoding;
	double gain;
	static void set_stereo();
	VideoReader();
	VideoReader( VideoKind video_kind, int device, int camera_width, int camera_height, int xoffset = 0, int yoffset = 0,
	             int image_width = 0, int image_height = 0, int exposure = INT_MAX, double gain = 1, bool gamma_encoding = true );
	VideoReader( VideoKind video_kind, const char file[] );
	void open( VideoKind video_kind, int device, int camera_width, int camera_height, int xoffset = 0, int yoffset = 0,
	           int image_width = 0, int image_height = 0, int exposure = INT_MAX, double gain = 1, bool gamma_encoding = true );
	void open( VideoKind video_kind, const char file[] );
	bool reset();
	bool is_open();
	void set_exposure( int exposure );
	void set_gain( double gain );
	int get_width();
	int get_height();
	int get_rows();
	int get_cols();
	unsigned long get_frame_number();
	bool grab_frame();
	bool retrieve_frame( cv::Mat &image );
	bool read_frame( cv::Mat &image );
	bool is_real_time();
	bool eos();
	void close();
	~VideoReader();
};

#endif