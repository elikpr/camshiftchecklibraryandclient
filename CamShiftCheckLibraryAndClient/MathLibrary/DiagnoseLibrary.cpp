// MathLibrary.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include "DiagnoseLibrary.h"
#include "VideoIO.h"
#include <stdio.h>
#include "enumser.h"

using namespace cv;
using namespace std;

struct Side_Images
{
	Mat imageT;
	Mat imageR;
	Mat imageL;
	Mat imageSR;
	Mat imageSL;
	bool T = true;
	bool R = true;
	bool L = true;
	bool SR = true;
	bool SL = true;
};

class FrameAverage
{
	public:
		const int buff_size = 60;
		int idx;
		std::vector<cv::Mat> buff;
		cv::Mat sum_image, avg_image;

		FrameAverage(int rows, int cols)
			: buff(buff_size), idx(0), sum_image(rows, cols, CV_32SC3, cv::Scalar::all(0)), avg_image(rows, cols, CV_32SC3)
		{
			for (int i = 0; i < buff_size; ++i) buff[i].create(rows, cols, CV_32SC3);
		}

		void insert_image(const cv::Mat &image)
		{
			image.convertTo(buff[idx], CV_32SC3);
			sum_image += buff[idx];
			idx = (idx + 1) % buff_size;
		}

		void sum_avg(cv::Mat &currFrame)
		{
			sum_image -= buff[idx];
			currFrame.convertTo(buff[idx], CV_32SC3);
			sum_image += buff[idx];
			idx = (idx + 1) % buff_size;

			avg_image = sum_image / buff_size;
			avg_image.convertTo(currFrame, CV_8UC3);
		}
};

void writeToPort(ofstream &fout, unsigned char c1, unsigned char c2, int port)
{
	HANDLE hComm;
	string port_ = to_string(port);
	port_ = "COM" + port_;

	// converting string to tchar
	size_t size = port_.size() + 1;
	TCHAR *pcCommPort = new TCHAR[size];
	pcCommPort[size - 1] = 0;
	for (size_t i = 0; i < size - 1; ++i)
		pcCommPort[i] = (port_[i]);

	// Opening serial port
	hComm = CreateFile(pcCommPort,     //port name
		GENERIC_READ | GENERIC_WRITE, //Read/Write
		0,                            // No Sharing
		NULL,                         // No Security
		OPEN_EXISTING,// Open existing port only
		0,            // Non Overlapped I/O
		NULL);        // Null for Comm Devices

	if (hComm == INVALID_HANDLE_VALUE) 
	{
		//printf("Error in opening serial port: %x\n", GetLastError());
		fout << "Error in opening serial port: " << GetLastError() << endl << flush;
	}
	else
	{
		//printf("opening serial port successfully\n");
		fout << "opening serial port successfully" << endl << flush;
		DCB dcbSerialParams = { 0 }; // Initializing DCB structure
		dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

		bool Status = GetCommState(hComm, &dcbSerialParams);

		dcbSerialParams.BaudRate = CBR_9600;  // Setting BaudRate = 9600
		dcbSerialParams.ByteSize = 8;         // Setting ByteSize = 8
		dcbSerialParams.StopBits = ONESTOPBIT;// Setting StopBits = 1
		dcbSerialParams.Parity = NOPARITY;  // Setting Parity = None

		SetCommState(hComm, &dcbSerialParams);

		DWORD dNoOFBytestoWrite;         // No of bytes to write into the port
		DWORD dNoOfBytesWritten = 0;     // No of bytes written to the port
		dNoOFBytestoWrite = sizeof(c1);

		Status = WriteFile(hComm,        // Handle to the Serial port
			(LPCVOID)&c1,     // Data to be written to the port
			dNoOFBytestoWrite,  //No of bytes to write
			&dNoOfBytesWritten, //Bytes written
			NULL);
		if (Status) 
		{
			//printf("written\n");
			fout << "Written\n" << flush;
		}
		else 
		{
			//printf("problem with writing\n");
			fout << "problem with writing\n" << flush;
		}

		Sleep(0);

		Status = WriteFile(hComm,        // Handle to the Serial port
			(LPCVOID)&c2,     // Data to be written to the port
			dNoOFBytestoWrite,  //No of bytes to write
			&dNoOfBytesWritten, //Bytes written
			NULL);
		if (Status)
		{
			//printf("written\n");
			fout << "Written\n" << flush;
		}
		else
		{
			//printf("problem with writing\n");
			fout << "problem with writing\n" << flush;
		}
	}

	CloseHandle(hComm);//Closing the Serial Port
}

int StringToWString(std::wstring &ws, const std::string &s)
{
	std::wstring wsTmp(s.begin(), s.end());
	ws = wsTmp;
	return 0;
}

int resetStereo(ofstream &fout)
{
	//Initialize COM (Required by CEnumerateSerial::UsingWMI)
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		//_tprintf(_T("Failed to initialize COM, Error:%x\n"), hr);
		fout << "Failed to initialize COM, Error: " << hr << endl << flush;
		return -1;
	}

	CEnumerateSerial::CPortsArray ports;
	CEnumerateSerial::CNamesArray names;

	#ifdef CENUMERATESERIAL_USE_STL
		size_t i = 0;
		UNREFERENCED_PARAMETER(i);
	#elif defined _AFX
		INT_PTR i = 0;
	#else
		int i = 0;
	#endif

	if (CEnumerateSerial::UsingSetupAPI1(ports, names))
	{
		const char key[] = "Prolific USB-to-Serial Comm Port";
		string check(key);
		wstring wkey;
		StringToWString(wkey, check);
		#ifdef CENUMERATESERIAL_USE_STL
				bool onlyOnce = false;
				for (i = 0; i < ports.size(); i++)
					if (names[i].compare(wkey) == 0 && !onlyOnce)
					{
						onlyOnce = true;
						//_tprintf(_T("COM%u <%s>\n"), ports[i], names[i].c_str());
						fout << "COM" << ports[i] << " " << names[i].c_str() << endl << flush;
						unsigned char command1 = 0x64, command2 = 0x74;
						writeToPort(fout, command1, command2, ports[i]);
					}
		#else
			for (i = 0; i<ports.GetSize(); i++)
				_tprintf(_T("COM%u <%s>\n"), ports[i], names[i].operator LPCTSTR());
		#endif
	}
	else 
	{
		//_tprintf(_T("CEnumerateSerial::UsingSetupAPI1 failed, Error:%u\n"), GetLastError());
		fout << "CEnumerateSerial::UsingSetupAPI1 failed, Error: " << GetLastError() << endl << flush;
	}

	return 0;
}

void discard_images(VideoReader &video_reader0, VideoReader &video_reader1, int num_frames)
{
	int rows = video_reader0.get_rows();
	int cols = video_reader0.get_cols();

	cv::Mat image0(rows, cols, CV_8UC3);
	cv::Mat image1(rows, cols, CV_8UC3);

	for (int n = 0; n < num_frames; ++n)
	{
		video_reader0.read_frame(image0);
		video_reader1.read_frame(image1);
	}
}

void write_result(double radiusT, double radiusR, double radiusL, int radiusSR, int radiusSL, int overallResultMono, int overallResultStereo)
{
	//file.open("test.txt", fstream::out);
	//std::ofstream fout("camShiftResult.txt");
	std::ofstream fout;
	fout.open("camShiftResult.txt", fstream::out);
	fout << radiusT << std::endl << radiusR << std::endl << radiusL << std::endl 
		 << radiusSR << std::endl << radiusSL << std::endl 
		 << overallResultMono << std::endl << overallResultStereo;
	fout.close();
}

void read_device_numbers(const char file_in_device_numbers[], int &camT, int &camR, int &camL, int &camSR, int &camSL)
{
	std::ifstream fin(file_in_device_numbers);

	if (fin.fail())
	{
		std::cerr << "Error: " << file_in_device_numbers << " not found.\n";
		exit(EXIT_FAILURE);
	}

	fin >> camSR >> camSL >> camT >> camR >> camL;

	fin.close();
}

bool is_stable_connection(ofstream &fout, VideoReader &video_readerSR, VideoReader &video_readerSL, int camSR, int camSL)
{
	bool result = false;
	const int cols = 1280;
	const int rows = 960;
	int exposure = 2000, trying = 0;
	cv::Mat imageSR, imageSL;
	fout << "Checking connection starts\n" << flush;
	for (int i = 0; i < 3; i++) // Tries 3 times to open camera if camera fails to open
	{
		video_readerSR.open(VideoReader::HONTEK, camSR, cols, rows, 0, 0, 0, 0, exposure, 0, 0);
		video_readerSL.open(VideoReader::HONTEK, camSL, cols, rows, 0, 0, 0, 0, exposure, 0, 0);
		if (video_readerSR.eos() || video_readerSL.eos())
		{
			fout << "CamOpenError\n" << flush;
			video_readerSR.close();
			video_readerSL.close();
			Sleep(10000);
		}
		else 
		{
			int j;
			for (j = 0; j < 5; j++)
			{
				video_readerSR.read_frame(imageSR);
				video_readerSL.read_frame(imageSL);
				if (video_readerSR.eos() || video_readerSL.eos())
				{
					video_readerSR.close();
					video_readerSL.close();
					fout << "CamReadError\n" << flush;
					Sleep(10000);
					j = 0;
					break;
				}
			}
			fout << "j = " << j << endl << flush;
			if (j == 5) 
			{
				fout << "---CamFINE---\n" << flush;
				result = true;
				//i = 3;
				break;
			}
		}
		if (!result && i == 2)
		{
			if (trying < 2 && resetStereo(fout) == -1) { Sleep(10000); i == 1; trying++; }
			else { Sleep(10000); }
		}
	}
	fout << "Checking connection ends\n" << flush;
	return result;
}

Side_Images get_curr_img(ofstream &fout, int camT, int camR, int camL, int camSR, int camSL)
{
	const int height = 480;
	const int width = 640;

	const int cols = 1280;
	const int rows = 960;

	Mat imageT, imageR, imageL, imageSR = Mat::zeros(rows, cols, CV_8UC3), imageSL = Mat::zeros(rows, cols, CV_8UC3);
	imageT.setTo(0); imageR.setTo(0); imageL.setTo(0)/*, imageSR.setTo(0); imageSL.setTo(0)*/;
	bool T = true, R = true, L = true, SR = true, SL = true;
	Side_Images side;
	
	////
	VideoReader video_readerT, video_readerR, video_readerL, video_readerSR, video_readerSL;

	FrameAverage frameaverageT(height, width), frameaverageR(height, width), frameaverageL(height, width), 
				 frameaverageSR(rows, cols), frameaverageSL(rows, cols);

	//Setting Stereo cams
	//int exposure = 2000;
	double t1 = cv::getTickCount();
	// openning camera
	if ( camT >= 0 )	video_readerT.open(VideoReader::STANDARD, camT, width, height, 0, 0, 0, 0);
	if ( camR >= 0 )	video_readerR.open(VideoReader::STANDARD, camR, width, height, 0, 0, 0, 0);
	if ( camL >= 0 )	video_readerL.open(VideoReader::STANDARD, camL, width, height, 0, 0, 0, 0);
	//if ( camSR >= 0)	video_readerSR.open(VideoReader::HONTEK, camSR, cols, rows, 0, 0, 0, 0, exposure, 0, 0);
	//if ( camSL >= 0)	video_readerSL.open(VideoReader::HONTEK, camSL, cols, rows, 0, 0, 0, 0, exposure, 0, 0);
	
	bool connection = is_stable_connection(fout, video_readerSR, video_readerSL, camSR, camSL);
	if (!connection) 
	{ 
		SR = false; 
		SL = false; 		
	}
	
	double t2 = cv::getTickCount();
	double time = (t2 - t1) / cv::getTickFrequency();	
	fout << "video_reader.open() time: " << time << std::endl << std::flush;
	
	if (!connection) SR = false;
	if (!connection) SL = false;
	
	if (SR == true && SL == true)
	{
		double t5 = cv::getTickCount();
		//Skips 10 frames from stereo cams
		discard_images(video_readerSR, video_readerSL, 20);

		double t6 = cv::getTickCount();
		double time_dis = (t6 - t5) / cv::getTickFrequency();
		fout << "discard_images() time: " << time_dis << std::endl << std::flush;

		double t3 = cv::getTickCount();
		for (int n = 0; n < frameaverageT.buff_size; ++n)
		{
			if (camT >= 0)	video_readerT.read_frame(imageT);
			if (camR >= 0)	video_readerR.read_frame(imageR);
			if (camL >= 0)	video_readerL.read_frame(imageL);
			if (camSR >= 0 && SR) { video_readerSR.read_frame(imageSR); if (video_readerSR.eos()) SR = false; }
			if (camSL >= 0 && SL) { video_readerSL.read_frame(imageSL); if (video_readerSL.eos()) SL = false; }

			if (camT >= 0)	frameaverageT.insert_image(imageT);
			if (camR >= 0)	frameaverageR.insert_image(imageR);
			if (camL >= 0)	frameaverageL.insert_image(imageL);
			if (camSR >= 0 && SR)	frameaverageSR.insert_image(imageSR);
			if (camSL >= 0 && SL)	frameaverageSL.insert_image(imageSL);
		}

		if (camT >= 0)	video_readerT.read_frame(imageT);
		if (camR >= 0)	video_readerR.read_frame(imageR);
		if (camL >= 0)	video_readerL.read_frame(imageL);
		if (camSR >= 0 && SR) { video_readerSR.read_frame(imageSR); if (video_readerSR.eos()) SR = false; }
		if (camSL >= 0 && SL) { video_readerSL.read_frame(imageSL); if (video_readerSL.eos()) SL = false; }

		// Calculating average sum	
		if (camT >= 0)		frameaverageT.sum_avg(imageT);
		if (camR >= 0)		frameaverageR.sum_avg(imageR);
		if (camL >= 0)		frameaverageL.sum_avg(imageL);
		if (camSR >= 0 && SR)	frameaverageSR.sum_avg(imageSR);
		if (camSL >= 0 && SL)	frameaverageSL.sum_avg(imageSL);
		double t4 = cv::getTickCount();
		double time1 = (t4 - t3) / cv::getTickFrequency();
		fout << "averaging time: " << time1 << std::endl << std::flush;

		side.imageT = imageT;
		side.imageR = imageR;
		side.imageL = imageL;
		side.imageSR = imageSR;
		side.imageSL = imageSL;

		video_readerSR.close();
		video_readerSL.close();
	}
	else
		fout << "SR SL problem\n" << flush;

	video_readerT.close(); 
	video_readerR.close(); 
	video_readerL.close();  

	side.T = T;
	side.R = R;
	side.L = L;
	side.SR = SR;
	side.SL = SL;

	return side;
}

Side_Images get_bg_img(int camT, int camR, int camL, int camSR, int camSL)
{
	Side_Images side;
	////	
	char file_in_backgroundT[256] =  "C:\\SensorData\\CalibDataTopSide\\backgroundT.png";
	char file_in_backgroundR[256] =  "C:\\SensorData\\CalibDataTopSide\\backgroundR.png";
	char file_in_backgroundL[256] =  "C:\\SensorData\\CalibDataTopSide\\backgroundL.png";
	char file_in_backgroundSR[256] = "C:\\SensorData\\CalibDataStereo\\background0.png";
	char file_in_backgroundSL[256] = "C:\\SensorData\\CalibDataStereo\\background1.png";
	
	cv::Mat backgroundT, backgroundR, backgroundL, backgroundSR, backgroundSL;
	
	backgroundT.setTo(0);
	backgroundR.setTo(0);
	backgroundL.setTo(0);
	backgroundSR.setTo(0);
	backgroundSL.setTo(0);
	
	if (camT >= 0)
		backgroundT = cv::imread(file_in_backgroundT, CV_LOAD_IMAGE_UNCHANGED);
	if (camR >= 0)
		backgroundR = cv::imread(file_in_backgroundR, CV_LOAD_IMAGE_UNCHANGED);
	if (camL >= 0)
		backgroundL = cv::imread(file_in_backgroundL, CV_LOAD_IMAGE_UNCHANGED);
	if (camSR >= 0)
		backgroundSR = cv::imread(file_in_backgroundSR, CV_LOAD_IMAGE_UNCHANGED);
	if (camSL >= 0)
		backgroundSL = cv::imread(file_in_backgroundSL, CV_LOAD_IMAGE_UNCHANGED);

	//if (backgroundT.empty() || backgroundR.empty() && backgroundL.empty()) { std::cerr << "background files not found\n"; exit(1); }
	////
	side.imageT = backgroundT;
	side.imageR = backgroundR;
	side.imageL = backgroundL;
	side.imageSR = backgroundSR;
	side.imageSL = backgroundSL;
	
	return side;
}

double calc_Correlation(Mat bg, Mat frm, bool IMAGE_SHOW, string side)
{
	Mat frame, curr, prev, curr64f, prev64f, hann, diffs;

	prev = bg;		//backgrounds.imageT;
	frame = frm;	//currs.imageT;

	cvtColor(frame, curr, COLOR_BGR2GRAY);  //COLOR_RGB2GRAY
	cvtColor(prev,  prev, COLOR_BGR2GRAY);  //COLOR_RGB2GRAY

	createHanningWindow(hann, curr.size(), CV_64F);

	prev.convertTo(prev64f, CV_64F);
	curr.convertTo(curr64f, CV_64F);

	Point2d shift = phaseCorrelate(prev64f, curr64f, hann);
	double radius = std::sqrt(shift.x*shift.x + shift.y*shift.y);
	//cout << "shift.x = " << ceil(shift.x) << "; \nshift.y = " << ceil(shift.y) << "; \nradius = " << radius << endl;
	//cout << "-------------------------\n";
	if (radius > 2)
	{
		// draw a circle and line indicating the shift direction...
		Point center(curr.cols >> 1, curr.rows >> 1);
		circle(frame, center, (int)radius, Scalar(0, 255, 0), 1, LINE_AA);
		line(frame, center, Point(center.x + (int)shift.x, center.y + (int)shift.y), Scalar(0, 255, 0), 1, LINE_AA);
	}

	absdiff(prev, curr, diffs);
	cv::String imageName = "diffs" + side + ".png";
	imwrite(imageName, diffs);
	Sleep(0);

	return radius;
}

//--------------------------------------------------------------------------------------------------------//
int __stdcall calc_cam_shift( bool SHOW_IMAGE )
{
	int res = 0, resT = 0, resR = 0, resL = 0, resSR = 0, resSL = 0, resS = 0;
	Side_Images backgrounds, currs;
	ofstream fout;
	remove("Diagnosis_LOG.txt");
	fout.open("Diagnosis_LOG.txt");	//, fstream::out
	fout << "----START DIAGNOSIS----" << endl << flush;

	double t_start = cv::getTickCount();
	int camT, camR, camL, camSR, camSL;
	char file_in_device_numbers[] = "D:/SensorData/DeviceNumbers/DeviceNumbers.txt";
	// Reads device port numbers from a file
	read_device_numbers( file_in_device_numbers, camT, camR, camL, camSR, camSL );
	double t0 = cv::getTickCount();
	// Gets background images
	backgrounds = get_bg_img( camT, camR, camL, camSR, camSL );// imread("room0\\backgroundT_Old.png");
	double t1 = cv::getTickCount();
	double time_get_bg_img = (t1 - t0) / cv::getTickFrequency();	
	fout << "get_bg_img() time" << " : " << time_get_bg_img << std::endl << std::flush;	

	double t2 = cv::getTickCount();	
	// Gets current averaged images
	currs = get_curr_img(fout, camT, camR, camL, camSR, camSL );// imread("room0\\backgroundT.png");
	double t3 = cv::getTickCount();
	double time_curr_img = (t3 - t2) / cv::getTickFrequency();
	fout << "time_curr_img() time" << " : " << time_curr_img << std::endl << std::flush;

	double radiusT = 0, radiusR = 0, radiusL = 0, radiusSR = 0, radiusSL = 0;
	
	double t4 = cv::getTickCount();
	// Calculate phase correlation for each Mono cameras
	if ( camT >= 0 )				radiusT = calc_Correlation( backgrounds.imageT, currs.imageT, SHOW_IMAGE, "T" );
	if ( camR >= 0 )				radiusR = calc_Correlation( backgrounds.imageR, currs.imageR, SHOW_IMAGE, "R" );
	if ( camL >= 0 )				radiusL = calc_Correlation( backgrounds.imageL, currs.imageL, SHOW_IMAGE, "L" );
	if ( camSR >= 0 && currs.SR && currs.SL)	radiusSR = calc_Correlation(backgrounds.imageSR, currs.imageSR, SHOW_IMAGE, "SR");
	if ( camSL >= 0 && currs.SR && currs.SL)	radiusSL = calc_Correlation(backgrounds.imageSL, currs.imageSL, SHOW_IMAGE, "SL");
	double t5 = cv::getTickCount();
	double time_calc_Correlation = (t5 - t4) / cv::getTickFrequency();
	fout << "time_calc_Correlation() time" << " : " << time_calc_Correlation << std::endl << std::flush;

	if ( radiusT > 2 )		resT = 1;
	if ( radiusR > 0.8 )	resR = 1;
	if ( radiusL > 0.8 )	resL = 1;
	if ( radiusSR > 20 )	resSR = 1; //1-shift; 2-disconnection
	if ( radiusSL > 20 )	resSL = 1; //1-shift; 2-disconnection
	if (camSR >= 0 && currs.SR == false) resSR = 2; //0-no problem; 1-shift; 2-disconnection;
	if (camSL >= 0 && currs.SL == false) resSL = 2; //0-no problem; 1-shift; 2-disconnection;

	res  = ( resT  || resR || resL );
	resS = (resSR == 2 || resSL == 2) ? 2 : ( resSR || resSL );
	// Write down results to a file
	write_result(radiusT, radiusR, radiusL, radiusSR, radiusSL, res, resS);
	
	double t_end = cv::getTickCount();
	double time_calc_cam_shift = (t_end - t_start) / cv::getTickFrequency();	
	fout << "calc_cam_shift() time" << " : " << time_calc_cam_shift << std::endl << std::flush;
	fout << "----END DIAGNOSIS----" << endl << flush;
	fout.close();
	
	return res;
}

void __stdcall imgshow()
{
	cv::Mat img = cv::imread("images.jpg");
	cv::imshow("image", img); cv::waitKey();
}

int __stdcall backgroundLearn()
{
	Side_Images new_backgrounds, old_backgrounds;
	ofstream fout;
	fout.open("Diagnosis_LOG.txt", std::ios::app);
	fout << "----START BK LEARN----" << endl << flush;

	int camT, camR, camL, camSR, camSL;
	char file_in_device_numbers[] = "D:/SensorData/DeviceNumbers/DeviceNumbers.txt";
	// Reads devices' port numbers from a file
	read_device_numbers(file_in_device_numbers, camT, camR, camL, camSR, camSL);

	// Rename current backgrounds
	int result = 0;
	if (camT >= 0)
	{	
		remove("C:/SensorData/CalibDataTopSide/backgroundT_old.png");
		char oldnameT[] = "C:/SensorData/CalibDataTopSide/backgroundT.png";
		char newnameT[] = "C:/SensorData/CalibDataTopSide/backgroundT_old.png";
		result = rename(oldnameT, newnameT);
		if (result != 0) return -1;
	}

	if(camR >= 0)
	{
		remove("C:/SensorData/CalibDataTopSide/backgroundR_old.png");
		char oldnameR[] = "C:/SensorData/CalibDataTopSide/backgroundR.png";
		char newnameR[] = "C:/SensorData/CalibDataTopSide/backgroundR_old.png";
		result = rename(oldnameR, newnameR);
		if (result != 0) return -1;
	}

	if (camL >= 0)
	{
		remove("C:/SensorData/CalibDataTopSide/backgroundL_old.png");
		char oldnameL[] = "C:/SensorData/CalibDataTopSide/backgroundL.png";
		char newnameL[] = "C:/SensorData/CalibDataTopSide/backgroundL_old.png";
		result = rename(oldnameL, newnameL);
		if (result != 0) return -1;
	}

	// Background Learning && saving
	new_backgrounds = get_curr_img(fout, camT, camR, camL, camSR, camSL);
	if (camT >= 0)
	{
		imwrite("C:/SensorData/CalibDataTopSide/backgroundT.png", new_backgrounds.imageT);
	}
	if (camR >= 0)
	{
		imwrite("C:/SensorData/CalibDataTopSide/backgroundR.png", new_backgrounds.imageR);
	}
	if (camL >= 0)
	{
		imwrite("C:/SensorData/CalibDataTopSide/backgroundL.png", new_backgrounds.imageL);
	}

	fout << "----END BK LEARN----" << endl << flush;
	fout.close();
	return result;
}