#ifndef __HVRLIBEX_H__
#define __HVRLIBEX_H__

///////////////////////////////////////////////
// external 변수 및 함수...

//Bayer OUTPUT MODE
#define	BGGR				1	
#define RGGB				2	
#define GBRG				3	
#define GRBG				4	
#define BlackWhite			5

#define HVR2030R	0
#define HVR2300R	1
#define HVR2130P	2
#define HVR2200P	3
#define HVR2300P	4
#define HVR2036R	5
#define HVR2500R	6

#define R640	0
#define R720	1
#define R960	2

#define USB3	3

#define MODE_RECPLAY	2
#define MODE_PLAY		1
#define MODE_STOP		0

#define MODE_1_EXPOSURE	4
#define MODE_1_SHUTTER	5
#define MODE_1_HBLANK	6
#define MODE_1_VBLANK	7
#define MODE_1_TRIGGER	8
#define MODE_1_USBSPEED	9
#define MODE_1_AEG		10
#define MODE_1_READ		11
#define MODE_1_WRITE	12

#define MODE_2_GAIN		9

extern "C" __declspec(dllexport) void __stdcall HVR_Set_ISP(IAMCameraControl *pCameraControl, int mode, LONG lvalue, LONG *lMin, LONG *lMax, LONG *lStep, LONG *gDrvIndex, LONG *lPropertyRangeFlags);
extern "C" __declspec(dllexport) void __stdcall HVR_Set_ISP2(IAMVideoProcAmp *pCameraControl, int mode, LONG value, LONG *lvalue);
extern "C" __declspec(dllexport) void __stdcall BmpSmoothingMask(BYTE *pBuf, LONG	lmSmoothing, unsigned int nWidth, unsigned int nHeight);
extern "C" __declspec(dllexport) void __stdcall BmpSharpening(BYTE *pBuf, LONG lmSharpening, unsigned int nWidth, unsigned int nHeight);
extern "C" __declspec(dllexport) void __stdcall BmpThreshold(BYTE *pBuf, DWORD lmThreshold, unsigned int nWidth, unsigned int nHeight);
extern "C" __declspec(dllexport) void __stdcall CB_Init(double Brightness, double Contrast, BYTE *CBBuf);
extern "C" __declspec(dllexport) void __stdcall BmpContrast(BYTE *CBBuf, BYTE *pBuf, unsigned int nWidth, unsigned int nHeight);

void  statusUpdateStatus(HWND hwnd, LPCTSTR lpsz);
void FAR PASCAL BMP_To_JPG(char *Source, char *Target);

#endif  // __HVRLIBEX_H__