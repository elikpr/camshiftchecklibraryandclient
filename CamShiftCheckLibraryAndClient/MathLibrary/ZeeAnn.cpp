#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#pragma comment( lib, "ZADL.lib" )
#pragma comment( lib, "ZADR.lib" )

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/core/core_c.h>
#include "ZADLInterfaceL.h"
#include "ZADLInterfaceR.h"
#include "ZeeAnn.h"

namespace ZeeAnn {

const int IMAGE_FORMAT_ISPBAYER = 1;

void connect_mono( int device, int exposure, double gain, bool gamma_encoding )
{
	const int dgain = 1;
	char data;

	if( device == 0 )
	{
		ZADRConnect( IMAGE_FORMAT_ISPBAYER );
		ZADRGPIOCtrl( 1, 0 );
		for( int i = 0; i < 4; ++i ) ZADRSensorInitial( IMAGE_FORMAT_ISPBAYER );

		// Turn off right auto exposure and auto white balance => 101XXXXX
		ZADRi2cWrite( 0x00, 0x02 );    // set page C
		ZADRi2cRead( 0x1B, &data );    // read adress 0x1B of page C
		data &= 0x1F;                  // 000XXXXX
		data |= 0xA0;                  // 101XXXXX
		ZADRi2cWrite( 0x1B, data );    // write a new value

		if( ! gamma_encoding )
		{
			// Turn off right gamma => XX0XXXXX
			ZADRi2cWrite( 0x00, 0x01 );    // set page B
			ZADRi2cRead( 0x09, &data );    // read address 0x09 of page B
			data &= 0xDF;                  // XX0XXXXX
			ZADRi2cWrite( 0x09, data );    // write a new value
		}

//		ZADRXOffset( ZeeAnn_offset_Rx );
//		ZADRYOffset( 480 - 400 - ZeeAnn_offset_Ry );

		ZADRExposureCtrl( exposure );
		ZADRGainCtrl( gain );
		ZADRDGainCtrl( dgain );
		ZADRGrabStart();
	}
	else if( device == 1 )
	{
		ZADLConnect( IMAGE_FORMAT_ISPBAYER );
		ZADLGPIOCtrl( 1, 0 );
		for( int i = 0; i < 4; ++i ) ZADLSensorInitial( IMAGE_FORMAT_ISPBAYER );

		// Turn off left auto exposure and auto white balance => 101XXXXX
		ZADLi2cWrite( 0x00, 0x02 );    // set page C
		ZADLi2cRead( 0x1B, &data );    // read adress 0x1B of page C
		data &= 0x1F;                  // 000XXXXX
		data |= 0xA0;                  // 101XXXXX
		ZADLi2cWrite( 0x1B, data );    // write a new value

		if( ! gamma_encoding )
		{
			// Turn off left gamma => XX0XXXXX
			ZADLi2cWrite( 0x00, 0x01 );    // set page B
			ZADLi2cRead( 0x09, &data );    // read address 0x09 of page B
			data &= 0xDF;                  // XX0XXXXX
			ZADLi2cWrite( 0x09, data );    // write a new value
		}

//		ZADLXOffset( ZeeAnn_offset_Lx );
//		ZADLYOffset( 480 - 400 - ZeeAnn_offset_Ly );

		ZADLExposureCtrl( exposure );
		ZADLGainCtrl( gain );
		ZADLDGainCtrl( dgain );
		ZADLGrabStart();
	}
	else
	{
		std::cerr << "The camera divice should be eather 0 or 1.\n";
		exit( -1 );
	}
}

void connect_stereo( int exposureR, int exposureL, double gain, bool gamma_encoding )
{
	const int dgain = 1;
	char data;

	ZADRConnect( IMAGE_FORMAT_ISPBAYER );
	ZADLConnect( IMAGE_FORMAT_ISPBAYER );

	ZADRGPIOCtrl( 1, 0 );
	ZADLGPIOCtrl( 1, 0 );
	
	for( int i = 0; i < 4; ++i )
	{
		ZADRSensorInitial( IMAGE_FORMAT_ISPBAYER );
		ZADLSensorInitial( IMAGE_FORMAT_ISPBAYER );
	}

	// Turn off right auto exposure and auto white balance => 101XXXXX
	ZADRi2cWrite( 0x00, 0x02 );    // set page C
	ZADRi2cRead( 0x1B, &data );    // read adress 0x1B of page C
	data &= 0x1F;                  // 000XXXXX
	data |= 0xA0;                  // 101XXXXX
	ZADRi2cWrite( 0x1B, data );    // write a new value

	// Turn off left auto exposure and auto white balance => 101XXXXX
	ZADLi2cWrite( 0x00, 0x02 );    // set page C
	ZADLi2cRead( 0x1B, &data );    // read adress 0x1B of page C
	data &= 0x1F;                  // 000XXXXX
	data |= 0xA0;                  // 101XXXXX
	ZADLi2cWrite( 0x1B, data );    // write a new value

	if( ! gamma_encoding )
	{
		// Turn off right gamma => XX0XXXXX
		ZADRi2cWrite( 0x00, 0x01 );    // set page B
		ZADRi2cRead( 0x09, &data );    // read address 0x09 of page B
		data &= 0xDF;                  // XX0XXXXX
		ZADRi2cWrite( 0x09, data );    // write a new value

		// Turn off left gamma => XX0XXXXX
		ZADLi2cWrite( 0x00, 0x01 );    // set page B
		ZADLi2cRead( 0x09, &data );    // read address 0x09 of page B
		data &= 0xDF;                  // XX0XXXXX
		ZADLi2cWrite( 0x09, data );    // write a new value
	}

//	ZADLXOffset( ZeeAnn_offset_Lx );
//	ZADLYOffset( 480 - 400 - ZeeAnn_offset_Ly );
//	ZADRXOffset( ZeeAnn_offset_Rx );
//	ZADRYOffset( 480 - 400 - ZeeAnn_offset_Ry );

	ZADRExposureCtrl( exposureR );
	ZADLExposureCtrl( exposureL );

	ZADRGainCtrl( gain );
	ZADLGainCtrl( gain );

	ZADRDGainCtrl( dgain );
	ZADLDGainCtrl( dgain );

	ZADRGrabStart();
	ZADLGrabStart();
}

void set_exposure( int device, int exposure )
{
	switch( device )
	{
	case 0: ZADRExposureCtrl( exposure );
	case 1: ZADLExposureCtrl( exposure );
	}
}

void set_gain( int device, double gain )
{
	switch( device )
	{
	case 0: ZADRGainCtrl( gain );
	case 1: ZADLGainCtrl( gain );
	}
}

void get_data( int device, uchar *buffer )
{
	switch( device )
	{
	case 0: ZADRgetLImageDump( buffer ); break;
	case 1: ZADLgetLImageDump( buffer ); break;
	}
}

void get_image( const uchar *buffer, int width, int height, int xoffset, int yoffset, cv::Mat &color_image )
{
	xoffset = ZeeAnn::width  - width  - xoffset;
	yoffset = ZeeAnn::height - height - yoffset;

	for( int i = 0; i < height; ++i )
	{
		const uchar *buffer_row = buffer + ( yoffset + i ) * ZeeAnn::width * 3;
		uchar *color_image_row = color_image.ptr<uchar>(i);

		for( int j = 0; j < width * 3; ++j )
		{
			color_image_row[j] = buffer_row[ xoffset * 3 + j ];
		}
	}

	cv::flip( color_image, color_image, -1 );
}

void disconnect( int device )
{
	if( device == 0 )
	{
		ZADRGrabStop();
		ZADRGPIOCtrl( 1, 1 );
		ZADRDisConnect();
	}
	else
	{
		ZADLGrabStop();
		ZADLGPIOCtrl( 1, 1 );
		ZADLDisConnect();
	}
}

void disconnect()
{
	ZADRGrabStop();
	ZADLGrabStop();

	ZADRGPIOCtrl( 1, 1 );
	ZADLGPIOCtrl( 1, 1 );

	ZADRDisConnect();
	ZADLDisConnect();
}

}