﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;

namespace CSHARPCLIENT_MathLibrary
{
    public partial class Form_Diagnosis : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        [DllImport(@"MathLibrary.dll", EntryPoint = "Add", CallingConvention = CallingConvention.StdCall)]
        public static extern int Add(int a, int b);

        [DllImport(@"MathLibrary.dll", EntryPoint = "imgshow", CallingConvention = CallingConvention.StdCall)]
        public static extern void imgshow();
        

        public Form_Diagnosis()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 5, 5));
                        
            int[] errorCode = new int[3];
            // get values from Diagnose.txt config file
            get_diagnose_result();            
        }
        private void get_diagnose_result()
        {
            int[] errorCode = new int[3];
            int counter = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader("DIAGNOSE.txt");
            while ((line = file.ReadLine()) != null)
            {
                errorCode[counter] = Int32.Parse(line);
                counter++;
            }
            file.Close();
            
            // call error illustrators
            if (errorCode[0] == 0) pictureBox_Conn.Load("true.png");
            else if (errorCode[0] == 1) pictureBox_Conn.Load("false.png");

            if (errorCode[1] == 0) pictureBox_Shift.Load("true.png");
            else if (errorCode[1] == 1) pictureBox_Shift.Load("false.png");

            if (errorCode[2] == 0) pictureBox_Noise.Load("true.png");
            else if (errorCode[2] == 1) pictureBox_Noise.Load("false.png");
        }

        private void btnDiagnose_Click(object sender, EventArgs e)
        {                        
            // Use opencv function
            backgroundWorker1.RunWorkerAsync();
            // get values from Diagnose.txt config file
            get_diagnose_result();
        }

        private void pictureBoxClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //imgshow();
            for (int i = 1; i <= 100; i++)
            {
                // Wait 100 milliseconds.
                Thread.Sleep(1000);
                // Report progress.
                backgroundWorker1.ReportProgress(i);
            }
        }             
    }
}
