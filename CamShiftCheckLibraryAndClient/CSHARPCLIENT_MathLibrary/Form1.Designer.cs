﻿namespace CSHARPCLIENT_MathLibrary
{
    partial class Form_Diagnosis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Diagnosis));
            this.btn_diagnose = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox_close = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.lblConnection = new System.Windows.Forms.Label();
            this.lblShift = new System.Windows.Forms.Label();
            this.lblNoise = new System.Windows.Forms.Label();
            this.pictureBox_Conn = new System.Windows.Forms.PictureBox();
            this.pictureBox_Shift = new System.Windows.Forms.PictureBox();
            this.pictureBox_Noise = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Conn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Shift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Noise)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_diagnose
            // 
            this.btn_diagnose.Location = new System.Drawing.Point(521, 332);
            this.btn_diagnose.Name = "btn_diagnose";
            this.btn_diagnose.Size = new System.Drawing.Size(66, 24);
            this.btn_diagnose.TabIndex = 0;
            this.btn_diagnose.Text = "Diagnose";
            this.btn_diagnose.UseVisualStyleBackColor = true;
            this.btn_diagnose.Click += new System.EventHandler(this.btnDiagnose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(600, 46);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(9, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 36);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(47, 10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(24, 23);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("MoeumT R", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(79, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Diagnosis";
            // 
            // pictureBox_close
            // 
            this.pictureBox_close.BackColor = System.Drawing.Color.White;
            this.pictureBox_close.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_close.Image")));
            this.pictureBox_close.Location = new System.Drawing.Point(566, 8);
            this.pictureBox_close.Name = "pictureBox_close";
            this.pictureBox_close.Size = new System.Drawing.Size(21, 24);
            this.pictureBox_close.TabIndex = 7;
            this.pictureBox_close.TabStop = false;
            this.pictureBox_close.Click += new System.EventHandler(this.pictureBoxClose_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 46);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 322);
            this.splitter1.TabIndex = 8;
            this.splitter1.TabStop = false;
            // 
            // lblConnection
            // 
            this.lblConnection.AutoSize = true;
            this.lblConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnection.Location = new System.Drawing.Point(35, 75);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(132, 26);
            this.lblConnection.TabIndex = 9;
            this.lblConnection.Text = "Connection";
            // 
            // lblShift
            // 
            this.lblShift.AutoSize = true;
            this.lblShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShift.Location = new System.Drawing.Point(35, 167);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(134, 26);
            this.lblShift.TabIndex = 10;
            this.lblShift.Text = "Image Shift";
            // 
            // lblNoise
            // 
            this.lblNoise.AutoSize = true;
            this.lblNoise.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoise.Location = new System.Drawing.Point(35, 259);
            this.lblNoise.Name = "lblNoise";
            this.lblNoise.Size = new System.Drawing.Size(73, 26);
            this.lblNoise.TabIndex = 11;
            this.lblNoise.Text = "Noise";
            // 
            // pictureBox_Conn
            // 
            this.pictureBox_Conn.Location = new System.Drawing.Point(260, 60);
            this.pictureBox_Conn.Name = "pictureBox_Conn";
            this.pictureBox_Conn.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_Conn.TabIndex = 12;
            this.pictureBox_Conn.TabStop = false;
            // 
            // pictureBox_Shift
            // 
            this.pictureBox_Shift.Location = new System.Drawing.Point(260, 150);
            this.pictureBox_Shift.Name = "pictureBox_Shift";
            this.pictureBox_Shift.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_Shift.TabIndex = 13;
            this.pictureBox_Shift.TabStop = false;
            // 
            // pictureBox_Noise
            // 
            this.pictureBox_Noise.Location = new System.Drawing.Point(260, 240);
            this.pictureBox_Noise.Name = "pictureBox_Noise";
            this.pictureBox_Noise.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_Noise.TabIndex = 14;
            this.pictureBox_Noise.TabStop = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // Form_Diagnosis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(600, 368);
            this.Controls.Add(this.pictureBox_Noise);
            this.Controls.Add(this.pictureBox_Shift);
            this.Controls.Add(this.pictureBox_Conn);
            this.Controls.Add(this.lblNoise);
            this.Controls.Add(this.lblShift);
            this.Controls.Add(this.lblConnection);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pictureBox_close);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_diagnose);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Diagnosis";
            this.Opacity = 0.9D;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Conn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Shift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Noise)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_diagnose;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox_close;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label lblConnection;
        private System.Windows.Forms.Label lblShift;
        private System.Windows.Forms.Label lblNoise;
        private System.Windows.Forms.PictureBox pictureBox_Conn;
        private System.Windows.Forms.PictureBox pictureBox_Shift;
        private System.Windows.Forms.PictureBox pictureBox_Noise;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

