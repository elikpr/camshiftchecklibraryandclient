﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows.Threading;
using System.Net.Mail;
using System.Collections.Generic;
using System.IO;

namespace Diagnosis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport(@"DiagnoseLibrary.dll", EntryPoint = "calc_cam_shift", CallingConvention = CallingConvention.StdCall)]
        public static extern int calc_cam_shift(bool SHOW_IMAGE);

        [DllImport(@"DiagnoseLibrary.dll", EntryPoint = "imgshow", CallingConvention = CallingConvention.StdCall)]
        public static extern void imgshow();

        [DllImport(@"DiagnoseLibrary.dll", EntryPoint = "backgroundLearn", CallingConvention = CallingConvention.StdCall)]
        public static extern int backgroundLearn();

        // Declaring backgorundWorkers
        private readonly BackgroundWorker worker  = new BackgroundWorker();
        private readonly BackgroundWorker worker1 = new BackgroundWorker();

        public MainWindow()
        {
            InitializeComponent();
        }
        //-----------------------------------------------------------------------------------------------------------------
        public void UpdateUIinfo()
        {            
            //Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));    
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new System.Threading.ThreadStart(delegate { lblInfo.Content = "Hold on a second..."; }));
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new System.Threading.ThreadStart(delegate { lblMentor.Content = "잠시만 기다려 주세요!..."; }));
        }

        string get_MID()
        {
            string MID = "";
            int counter = 0;
            string line;
            System.IO.StreamReader file = null;
            if (Directory.Exists(@"C:\SkillupGameClient"))
            {
                //MessageBox.Show("DONE1");
                file = new System.IO.StreamReader("C:/SkillupGameClient/config.txt");
            }
            else if (Directory.Exists(@"C:\YaguStarGameClient\"))
            {
                //MessageBox.Show("DONE2");
                file = new System.IO.StreamReader("C:/YaguStarGameClient/config.txt");
            }

            if (file == null) MessageBox.Show("cannot open MID file");

            while ((line = file.ReadLine()) != null)
            {
                MID = line;
                if (++counter == 8) break;
            }
            file.Close();

            if (MID == "") MessageBox.Show("Could not find config.txt");

            return MID;
        }

        private void get_diagnose_result()
        {
            List<string> radiuses = new List<string>();
            //string[] radiuses = new string[5];
            int counter = 0;
            string line;
            //System.IO.StreamReader file = new System.IO.StreamReader("DIAGNOSE.txt");
            System.IO.StreamReader file = new System.IO.StreamReader("camShiftResult.txt");
            while ((line = file.ReadLine()) != null)
            {
                radiuses.Add(line);
                counter++;
            }
            file.Close();

            lblTop.Content = radiuses[0];    // Mono Top camera
            lblRight.Content = radiuses[1];    // Mono Right camera
            lblLeft.Content = radiuses[2];    // Mono Left camera
            lblRightS.Content = radiuses[3];    // Stereo Right
            lblLeftS.Content = radiuses[4];    // Stereo Left
            //resultS = Int32.Parse(radiuses[4]);
            Int32.TryParse(radiuses[6], out resultS);
        }

        public void email_send()
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("part9techno@gmail.com");
            mail.To.Add("part9techno@gmail.com");
            mail.Subject = "ERROR REPORT from " + get_MID();
            mail.Body = "mail with attachment";

            System.Net.Mail.Attachment attachmentT, attachmentT_old,
                                       attachmentR, attachmentR_old,
                                       attachmentL, attachmentL_old,
                                       attachmentT_cal, attachmentR_cal, attachmentL_cal;

            attachmentT =     new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundT.png");
            attachmentT_old = new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundT_old.png");
            attachmentR =     new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundR.png");
            attachmentR_old = new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundR_old.png");
            attachmentL =     new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundL.png");
            attachmentL_old = new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundL_old.png");
            attachmentT_cal = new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundT_orig.png");
            attachmentR_cal = new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundR_orig.png");
            attachmentL_cal = new System.Net.Mail.Attachment("C:/SensorData/CalibDataTopSide/backgroundL_orig.png");

            mail.Attachments.Add(attachmentT);
            mail.Attachments.Add(attachmentT_old);
            mail.Attachments.Add(attachmentT_cal);
            mail.Attachments.Add(attachmentR);
            mail.Attachments.Add(attachmentR_old);
            mail.Attachments.Add(attachmentR_cal);
            mail.Attachments.Add(attachmentL);
            mail.Attachments.Add(attachmentL_old);
            mail.Attachments.Add(attachmentL_cal);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("part9techno@gmail.com", "skillup1007");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

        }

        int result = -1, resultS = -1, bkTraining = -1;
        //-----------------------------------------------------------------------------------------------------------------     
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            UpdateUIinfo();
            // run all background tasks here
            try
            {
                result = calc_cam_shift(true);               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {           
            //update ui once worker complete his work
            get_diagnose_result();
            
            LeftImg.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"diffsL.png", UriKind.Relative))
            };
            TopImg.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"diffsT.png", UriKind.Relative))
            };
            RightImg.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"diffsR.png", UriKind.Relative))
            };
            ImgSL.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"diffsSL.png", UriKind.Relative))
            };
            ImgSR.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"diffsSR.png", UriKind.Relative))
            };

            if (result == 1)      { lblInfo.Content = "There is a shift in Mono!"; lblInfo.Foreground = Brushes.Red;   }
            else if(result == 0)  { lblInfo.Content = "Mono Cameras are fine!";    lblInfo.Foreground = Brushes.Green; }

            if (resultS == 1)
            {
                lblInfoStereo.Content = "There is a shift in Stereo!";
                lblInfoStereo.Foreground = Brushes.Red;                
            }
            else if (resultS == 0)
            {                
                lblInfoStereo.Content = "Stereo Cameras are fine!";
                lblInfoStereo.Foreground = Brushes.Green;                
            }
            else if(resultS == 2)
            {
                lblInfoStereo.Content = "There is no connection Stereo!";
                lblInfoStereo.Foreground = Brushes.Red;
            }

            if(resultS == 2)
            {
                lblMentor.Content = "게임서버의 전원을 Off 했다가 다시 10초 후에 전원을 켜주세요.";
                btnDiagnose.IsEnabled = false;
                btnLearnBkd.IsEnabled = false;
            }
            else if(result == 1 || resultS == 1)
            {
                lblMentor.Content = "바닥의 공을 치우고 LearnBkd 버튼을 눌러주세요.";
                btnDiagnose.IsEnabled = true;
                btnLearnBkd.IsEnabled = true;
                btnClose.IsEnabled    = true;
            }
            else
            {
                lblMentor.Content = "카메라가 정상 작동합니다. 프로그램을 종료 해주세요.";
                btnDiagnose.IsEnabled = false;
                btnLearnBkd.IsEnabled = false;
                btnClose.IsEnabled    = true;
            }
        }

        private void worker1_DoWork(object sender, DoWorkEventArgs e)
        {
            UpdateUIinfo();
            try
            {
                bkTraining = backgroundLearn();
                email_send();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void worker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (bkTraining != 0)
            {
                lblInfo.Content = "There is a problem with BK Learning!";
                lblMentor.Content = "There is a problem with BK Learning!";
            }
            else
            {
                lblInfo.Content = "BK is trained!";
                lblMentor.Content = "Close 버튼을 눌러주세요!";
            }
            btnDiagnose.IsEnabled = true;
            btnLearnBkd.IsEnabled = true;
            btnClose.IsEnabled    = true;
            bkTraining = -1;
            result = -1;
            resultS = -1;
        }
        //-----------------------------------------------------------------------------------------------------------------
        private void btnDiagnose_Click(object sender, RoutedEventArgs e)
        {
            btnDiagnose.IsEnabled = false;
            btnLearnBkd.IsEnabled = false;
            btnClose.IsEnabled = false;

            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void btnLearnBkd_Click(object sender, RoutedEventArgs e)
        {
            btnDiagnose.IsEnabled = false;
            btnLearnBkd.IsEnabled = false;
            btnClose.IsEnabled = false;
            worker1.DoWork += worker1_DoWork;
            worker1.RunWorkerCompleted += worker1_RunWorkerCompleted;

            worker1.RunWorkerAsync();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("C:\\Sensor\\SensorManager.exe");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Close();
        }

    }
}
